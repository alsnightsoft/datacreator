package edu.pucmm.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by aluis on 8/31/16.
 */
public class Constants {

    private static final String DATE_FORMAT = "MM-dd-yyyy HH:mm:ss";
    private static final Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();

    public static <T> T convert(String data, Class<T> validClass) {
        if (data != null && !data.isEmpty()) {
            try {
                return gson.fromJson(data, validClass);
            } catch (Exception ignored) {
                return null;
            }
        }
        return null;
    }

    public static String stringify(Object object) {
        return gson.toJson(object);
    }
}
