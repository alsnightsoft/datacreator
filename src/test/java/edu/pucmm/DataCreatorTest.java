package edu.pucmm;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataCreatorTest {

    private File testFile = new File("Test.dat");

    @Test
    void test00Read() {
        assertEquals("Deben ser 0", 0, DataCreator.readData(testFile, Item.class).size());
    }

    @Test
    void test01SaveOne() {
        DataCreator.saveDataToFile(testFile, new Item("Naranja", 10, 25));
        assertEquals("Deben ser 1", 1, DataCreator.readData(testFile, Item.class).size());
    }

    @Test
    void test02ListOne() {
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item("Limon", 100, 12));
        itemList.add(new Item("Aguacate", 20, 40));
        DataCreator.saveDataToFile(testFile, itemList);
        assertEquals("Deben ser 3", 3, DataCreator.readData(testFile, Item.class).size());
    }

    @Test
    void test03ReplaceOne() {
        DataCreator.replaceDataToFile(testFile, new Item("Uva", 63, 215));
        assertEquals("Deben ser 1", 1, DataCreator.readData(testFile, Item.class).size());
    }

    @Test
    void test03ReplaceList() {
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item("Limon", 100, 12));
        itemList.add(new Item("Aguacate", 20, 40));
        itemList.add(new Item("Uva", 63, 215));
        itemList.add(new Item("Naranja", 10, 25));
        DataCreator.replaceDataToFile(testFile, itemList);
        assertEquals("Deben ser 4", 4, DataCreator.readData(testFile, Item.class).size());
    }

    private class Item {

        private String name;
        private int cant;
        private double price;

        public Item() {
        }

        public Item(String name, int cant, double price) {
            this.name = name;
            this.cant = cant;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCant() {
            return cant;
        }

        public void setCant(int cant) {
            this.cant = cant;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }
    }
}
